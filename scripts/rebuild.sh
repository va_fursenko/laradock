#!/usr/bin/env bash
sudo rm -Rf /var/lib/docker #runtime
docker rm -v -f $(docker ps -a -q)
docker-compose build --no-cache --force-rm --pull nginx postgres redis php-fpm workspace
##!/bin/sh